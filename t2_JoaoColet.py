arquivo = input()
f = open(arquivo, 'r')
texto = f.readlines()

strnumeros = "0123456789"
strletras = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
strletrasup = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

listalexema = [["if","IF"],["then","THEN"],["else","ELSE"],["end","END"],["repeat","REPEAT"],
			  ["until","UNTIL"],["read","READ"],["write","WRITE"],["+","PLUS"],["-","MINUS"],
			  ["*","TIMES"],["/","DIV"],["=","EQUAL"],["<","LESS"],["(","LBRACKET"],[")","RBRACKET"],
			  [";","DOTCOMA"],[":=","ATRIB"]]

listatoken = []

def verificatoken(lex, numlinha):
	for j in range (len(listalexema)):
		if lex == listalexema[j][0]:
			listatoken.append([listalexema[j][1], numlinha, lex])
			return 1
	return 0

def verificaNum(lex, numlinha):
	for letter in lex:
		if letter not in strnumeros:
			return 1
	listatoken.append(["NUM",numlinha, 'num'])
	return 0
	
def verificaId(lex, numlinha):	
	for letter in lex:
		if letter not in strletras and letter not in strnumeros:
			return 1
	listatoken.append(["ID",numlinha, 'id'])
	return 0

contlinha = 0
coment = 0
print("\n")
for linha in texto:
	contlinha += 1
	parte = linha.split()
	for i in parte:
		if i == "{" and coment==0:
			coment = 1
		elif i == "}" and coment==1:
			coment = 0
		elif coment == 0:
			x = verificatoken(i, contlinha)
			if x == 0 and i[0] in strnumeros:
				y = verificaNum(i, contlinha)
				if y == 1:
					print("Erro linha ",contlinha,": Numero invalido!")
			elif x == 0 and i[0] in strletras :
				y = verificaId(i, contlinha)
				if y == 1:
					print("Erro linha ",contlinha,": Identificador invalido!")
			elif x == 0 and i[0] not in strletras and i[0] not in strnumeros:
				print("Erro linha ",contlinha,": Identificador invalido!")
		
if coment == 1:
	print("Erro comentario nao finalizado!")

print("Lista de tokens:")
for l in range(len(listatoken)):		
	print('<',listatoken[l][0],',', listatoken[l][1],'>')				
	
#trabalho 2

tabsint = [["tabela sintatica preditiva","if","then","else","end","repeat","until","read","write",";","<","=","+","-","*","/","(",")","num","id","$"],
		  ["P","D","#","#","#","D","#","D","D","#","#","#","#","#","#","#","#","#","#","D","#"],
		  ["D","B X","#","#","#","B X","#","B X","B X","#","#","#","#","#","#","#","#","#","#","B X","#"],
		  ["X","#","#","&","&","#","&","#","#","; B X","#","#","#","#","#","#","#","#","#","#","&"],
		  ["B","if E then D J","#","#","#","repeat D until E","#","read id","write E","#","#","#","#","#","#","#","#","#","#","id := E","#"],
		  ["J","#","#","else D end","end","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#"],
		  ["E","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","H W","#","H W","H W","#"],
		  ["W","#","&","&","&","#","&","#","#","&","C H W","C H W","#","#","#","#","#","&","#","#","&"],
		  ["C","#","#","#","#","#","#","#","#","#","<","=","#","#","#","#","#","#","#","#","#"],
		  ["H","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","T Y","#","T Y","T Y","#"],
		  ["Y","#","&","&","&","#","&","#","#","&","&","&","S T Y","S T Y","#","#","#","&","#","#","&"],
		  ["S","#","#","#","#","#","#","#","#","#","#","#","+","-","#","#","#","#","#","#","#"],
		  ["T","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","F Z","#","F Z","F Z","#"],
		  ["Z","#","&","&","&","#","&","#","#","&","&","&","&","&","M F Z","M F Z","#","&","#","#","&"],
		  ["M","#","#","#","#","#","#","#","#","#","#","#","#","#","*","/","#","#","#","#","#"],
		  ["F","#","#","#","#","#","#","#","#","#","#","#","#","#","#","#","( E )","#","num","id","#"]]

entrada = [] #inicializa a palavra de entrada a ser analisada
for l in range(len(listatoken)):		
	entrada.append(listatoken[l][2])
entrada.append('$')
pilha=['$', 'P'] # inicializa a pilha


def empilhavariavel(M, a):
	s = ''
	for j in range(1,16): #encontra a linha da variavel 'M' desempilhada e a coluna do terminal 'a' no inicio da entrada 
		if tabsint[j][0] == M:
			for k in range(1, 21):
				if tabsint[0][k] == a:
					s = tabsint[j][k]
					print('[ '+M+' -> '+s+' ]')
	if s == '#': #se essa lina he coluna levou a # acusa erro do terminal ter ficado sem saida
		print('\n####### ERRO: Terminal sem saida #######')
		return 1
	elif s == '&': #se levou a palavra vazia apenas retorna sem empilhar nada
		return 0
	s = s.split()
	while len(s) > 0: #se levou a uma produçao qualquer empilha a palavra de forma invertida
		x = s.pop()
		pilha.append(x)
	return 0


def analisesint():
	while len(pilha) > 1: #realiza a analise até sobrar so o $ na pilha
		print(pilha, entrada)
		X = pilha.pop()    # desempilha
		if X not in strletrasup: # se o item desempilhado for um terminal pega o primeiro elemento da lista de entrada e compara se são iguais
			a = entrada.pop(0)
			if X != a: #se forem diferentes identifica o erro e ja finalzia a analise 
				print('\n####### ERRO: Pilha diferente da entrada #######') 
				return 1
		
		elif X in strletrasup: # se o i tem desempilhado for uma variavel chama a função que ira usar a tabela sintatica 
			e = empilhavariavel(X, entrada[0]) #os parametros passados sao a variavel desempilhada e o primeiro item na lista de entrada
			if e == 1: # se retornou 1 é porque o terminao nao levou a uma produção entao finaliza a analise
				return 1
		print('\n')
	print(pilha, entrada)
	X = pilha.pop()
	a = entrada.pop(0)
	if X == '$' and a == '$': #apos sobrar só o $ na pilha analisa se tambem sobrou só $ na entrada verificando se houve erro ou nao
		return 0
	else:
		print('\n####### ERRO: Pilha nao vazia #######')
		return 1

print('\nAnálise Sintática Preditiva Tabular\n[ PILHA ]  [ ENTRADA ] [ ACAO ]\n')	
asp = analisesint() #chama a função para fazer a analise sintatica
if asp == 1: 
	print('\nNAO ACEITO ')
elif asp == 0:
	print('\nACEITO')
			
f.close()